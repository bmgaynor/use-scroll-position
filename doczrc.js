export default {
  src: './lib',
  base: '/use-scroll-position/',
  dest: 'public',
  public: '/assets' // required to override this since default is the dest
}
